public class Board{
	private Die d1;
	private Die d2;
	private boolean[] tiles;
	
	//constructor
	public Board(){
		this.d1= new Die();
		this.d2= new Die();
		this.tiles = new boolean[12];
	}
	
	public boolean playATurn(){
		d1.roll();
		d2.roll();
		System.out.println(d1);
		System.out.println(d2);
		
		int sumOfDice=d1.getFaceValue()+d2.getFaceValue();
		if(this.tiles[sumOfDice-1]==false){
			this.tiles[sumOfDice-1]=true;
			System.out.println("Closing tile equal to sum: " +sumOfDice);
			return false;
			
		}else if(this.tiles[sumOfDice-1]==true && this.tiles[d1.getFaceValue()-	1]==false){
			this.tiles[d1.getFaceValue()-1]=true;
			System.out.println("Closing tile with the same value  as die one: " +d1.getFaceValue());
			return false;
			
		}else if(this.tiles[sumOfDice-1]==true && this.tiles[d1.getFaceValue()-1]==true && this.tiles[d2.getFaceValue()-1]==false){
			this.tiles[d2.getFaceValue()-1]=true;
			System.out.println("Closing tile with the same value  as die two: " +d2.getFaceValue());
			return false;
			
		}else if(this.tiles[sumOfDice-1]==true && this.tiles[d1.getFaceValue()-1]==true && this.tiles[d2.getFaceValue()-1]==true){
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
		
		return false;
	}
	
	//count point
	public String toString(){
		String s="";
		for(int i=0;i<this.tiles.length;i++){
			if(this.tiles[i]==true){
				s += " x";
			}else if(this.tiles[i]==false){
				s +=" ";
				s +=String.valueOf(i+1);
			}
		}
			
		return s;
	}
	
}