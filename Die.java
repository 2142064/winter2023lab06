public class Die{
	private int faceValue;
	private Rand random;

	//Constructor
	public Die(){
		this.faceValue=1;
		this.random = new Rand();
	}
	
	//initialize
	public void roll(){
		this.faceValue=this.random.getRandom();
	}
	
	//getter
	public int getFaceValue(){
		return this.faceValue;
	}
	
	public String toString(){
		return "Rolled: "+this.faceValue;
	}
}